package com.example.demo.invoice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvoiceService {
    private InvoiceRepository invoiceRepository;

    @Autowired
    public InvoiceService(InvoiceRepository repository) {
        this.invoiceRepository = repository;
    }

    public List<Invoice> retrieveInvoice() {
        return (List<Invoice>) invoiceRepository.findAll();
    }

    public Optional<Invoice> retrieveInvoice(Long id) {
        return invoiceRepository.findById(id);
    }

    public List<Invoice> retrieveInvoice(String Name) {
        return null;
    }

    public Invoice createInvoice(Invoice invoice) {

        return invoiceRepository.save(invoice);
    }

    public Optional<Invoice> updateInvoice(Long id, Invoice invoice) {
        Optional<Invoice> invoiceOptional = invoiceRepository.findById(id);
        if(!invoiceOptional.isPresent()) {
            return invoiceOptional;
        }

        return Optional.of(invoiceRepository.save(invoice));
    }

    public boolean deleteInvoice(Long id) {
        try {
            invoiceRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}