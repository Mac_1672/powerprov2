package com.example.demo.invoice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/invoices")
public class InvoiceController {

    @Autowired
    InvoiceService invoiceService;

    @GetMapping()
    public List<Invoice> getInvoice() {
        return invoiceService.retrieveInvoice();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getInvoice(@PathVariable Long id) {
        Optional<Invoice> invoice = invoiceService.retrieveInvoice(id);
        if(!invoice.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(invoice);
    }


    @GetMapping("/search")
    public List<Invoice> getInvoice(@RequestParam(value = "Name") String Name ) {
        return invoiceService.retrieveInvoice(Name);
    }

    @PostMapping()
    public ResponseEntity<?> postInvoice(@Valid @RequestBody Invoice body) {
        Invoice invoice = invoiceService.createInvoice(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(invoice);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putInvoice(@PathVariable Long id, @Valid @RequestBody Invoice body) {
        Optional<Invoice> invoice = invoiceService.updateInvoice(id, body);
        if(!invoice.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteInvoice(@PathVariable Long id) {
        if(!invoiceService.deleteInvoice(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}