package com.example.demo.invoice;

import org.springframework.data.repository.CrudRepository;


public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

}