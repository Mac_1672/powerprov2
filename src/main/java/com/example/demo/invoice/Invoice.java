package com.example.demo.invoice;
import com.example.demo.PaymentTerm.PaymentTerm;
import com.example.demo.user.User;
import com.example.demo.Status.Status;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Setter
@Getter
public class Invoice{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String invoiceNo;

    private String receiverName;

    private PaymentTerm paymentTerm;

    private String phoneNumber;

    private String address;

    private Double sumPrice;

    private Double vatPrice;

    private Double deliverFee;

    private Double coinDiscount;

    private Double promotionDiscount;

    private Double total;

    private Status status;

    private String QRCode;

//    @ManyToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "user_id", nullable = false)
//    private User user;

}