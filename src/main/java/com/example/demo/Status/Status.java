package com.example.demo.Status;

public enum  Status {
        WAITING,
        PAID,
        SENT,
        COMPLETED,
        VOIDED
}
