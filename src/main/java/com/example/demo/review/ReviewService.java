package com.example.demo.review;

import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@AllArgsConstructor
public class ReviewService {
    private ReviewRepository reviewRepository;


    public ReviewDTO save(ReviewDTO reviewDTO) {
        Review review = new Review();
        this.copyToEntity(review, reviewDTO);
        review = reviewRepository.save(review);
        return convertToReviewDTO(review);
    }

    public ReviewDTO convertToReviewDTO(Review review) {
        return ReviewDTO.builder()
                .id(review.getId())
                .Description(review.getDescription())
                .build();
    }

    public void copyToEntity(Review review, ReviewDTO dto) {
        review.setId(dto.getId());
        review.setDescription(dto.getDescription());
//        productTypeRepository.findById(dto.getProductTypeDTO().getId()).ifPresent(review::setProductType);
//        review.setActive(dto.getActive());
    }

    public Optional<Review> updateReview(Long id, Review review) {
        Optional<Review> reviewOptional = reviewRepository.findById(id);
        if(!reviewOptional.isPresent()) {
            return reviewOptional;
        }

        return Optional.of(reviewRepository.save(review));
    }

    public boolean deleteReview(Long id) {
        try {
            reviewRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}