package com.example.demo.review;

import com.example.demo.productType.ProductTypeDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sun.istack.NotNull;
import io.swagger.annotations.ApiModel;
import lombok.*;

import java.util.Date;


@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "CategoryDTO")
public class ReviewDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;

    @NotNull
    private String Description;

    private Double Rate;

    private Date DateTime;

}