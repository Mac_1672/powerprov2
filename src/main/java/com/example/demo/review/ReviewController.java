package com.example.demo.review;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/reviews")
public class ReviewController {

    @Autowired
    ReviewService reviewService;

//    @GetMapping()
//    public List<Category> getCategory() {
//        return categoryService.retrieveCategory();
//    }
//
//    @GetMapping("/{id}")
//    public ResponseEntity<?> getCategory(@PathVariable Long id) {
//        Optional<Category> category = categoryService.retrieveCategory(id);
//        if(!category.isPresent()) {
//            return ResponseEntity.notFound().build();
//        }
//        return ResponseEntity.ok(category);
//    }
//
//    @GetMapping("/search")
//    public List<Category> getCategory(@RequestParam(value = "Name") String Name ) {
//        return categoryService.retrieveCategory(Name);
//    }

//    @PostMapping()
//    public ResponseEntity<?> postCategoryDTO(@Valid @RequestBody CategoryDTO category) {
//        Category categoryDTO = categoryService.createCategory(category);
//        return ResponseEntity.status(HttpStatus.CREATED).body(category);

//        @PostMapping()
//        public ResponseEntity<?> postCategory(@Valid @RequestBody CategoryDTO categoryDTO) {
//            CategoryDTO categoryDTO1 = categoryService.save(categoryDTO);
//            if (categoryDTO1 == null) {
//                ResponseEntity.status(HttpStatus.CREATED).body(null);
//            }else {
//                return ResponseEntity.status(HttpStatus.CREATED).body(categoryDTO1);
//            }
//            return ResponseEntity.status(HttpStatus.CREATED).body(categoryDTO1);
//        }

    @PostMapping()
        public ResponseEntity<ReviewDTO>create(@Valid @RequestBody ReviewDTO reviewDTO) {
        reviewDTO = reviewService.save(reviewDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(reviewDTO);
    }






    @PutMapping("/{id}")
    public ResponseEntity<?> putReview(@PathVariable Long id, @Valid @RequestBody Review body) {
        Optional<Review> review = reviewService.updateReview(id, body);
        if(!review.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteReview(@PathVariable Long id) {
        if(!reviewService.deleteReview(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}