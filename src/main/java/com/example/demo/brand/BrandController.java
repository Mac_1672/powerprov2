package com.example.demo.brand;

import com.example.demo.category.CategoryDTO;
import com.example.demo.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/brands")
public class BrandController {

    @Autowired
    BrandService brandService;

//    @GetMapping()
//    public List<Category> getCategory() {
//        return categoryService.retrieveCategory();
//    }
//
//    @GetMapping("/{id}")
//    public ResponseEntity<?> getCategory(@PathVariable Long id) {
//        Optional<Category> category = categoryService.retrieveCategory(id);
//        if(!category.isPresent()) {
//            return ResponseEntity.notFound().build();
//        }
//        return ResponseEntity.ok(category);
//    }
//
//    @GetMapping("/search")
//    public List<Category> getCategory(@RequestParam(value = "Name") String Name ) {
//        return categoryService.retrieveCategory(Name);
//    }

//    @PostMapping()
//    public ResponseEntity<?> postCategoryDTO(@Valid @RequestBody CategoryDTO category) {
//        Category categoryDTO = categoryService.createCategory(category);
//        return ResponseEntity.status(HttpStatus.CREATED).body(category);

//        @PostMapping()
//        public ResponseEntity<?> postCategory(@Valid @RequestBody CategoryDTO categoryDTO) {
//            CategoryDTO categoryDTO1 = categoryService.save(categoryDTO);
//            if (categoryDTO1 == null) {
//                ResponseEntity.status(HttpStatus.CREATED).body(null);
//            }else {
//                return ResponseEntity.status(HttpStatus.CREATED).body(categoryDTO1);
//            }
//            return ResponseEntity.status(HttpStatus.CREATED).body(categoryDTO1);
//        }

    @PostMapping()
        public ResponseEntity<BrandDTO>create(@Valid @RequestBody BrandDTO brandDTO) {
        brandDTO = brandService.save(brandDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(brandDTO);
    }






    @PutMapping("/{id}")
    public ResponseEntity<?> putBrand(@PathVariable Long id, @Valid @RequestBody Brand body) {
        Optional<Brand> brand = brandService.updateBrand(id, body);
        if(!brand.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteBrand(@PathVariable Long id) {
        if(!brandService.deleteBrand(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}