package com.example.demo.brand;
import lombok.AllArgsConstructor;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;


import java.util.Optional;


@Service
@AllArgsConstructor
public class BrandService {
    private BrandRepository brandRepository;


    public BrandDTO save(BrandDTO brandDTO) {
        Brand brand = new Brand();
        this.copyToEntity(brand, brandDTO);
        brand = brandRepository.save(brand);
        return convertToBrandDTO(brand);
    }

    public BrandDTO convertToBrandDTO(Brand brand) {
        return com.example.demo.brand.BrandDTO.builder()
                .id(brand.getId())
                .Name(brand.getName())
                .build();
    }

    public void copyToEntity(Brand brand, com.example.demo.brand.BrandDTO dto) {
        brand.setId(dto.getId());
        brand.setName(dto.getName());
//        productTypeRepository.findById(dto.getProductTypeDTO().getId()).ifPresent(brand::setProductType);
//        brand.setActive(dto.getActive());
    }

    public Optional<Brand> updateBrand(Long id, Brand brand) {
        Optional<Brand> brandOptional = brandRepository.findById(id);
        if(!brandOptional.isPresent()) {
            return brandOptional;
        }

        return Optional.of(brandRepository.save(brand));
    }

    public boolean deleteBrand(Long id) {
        try {
            brandRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}