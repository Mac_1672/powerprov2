package com.example.demo.paymentconfirmation;

import com.example.demo.paymentconfirmation.Paymentconfirmation;
import org.springframework.data.repository.CrudRepository;

public interface PaymentconfirmationRepository extends CrudRepository<Paymentconfirmation, Long> {

}