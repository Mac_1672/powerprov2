package com.example.demo.paymentconfirmation;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Setter
@Getter

public class Paymentconfirmation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date transferDate;

     private Number amount;

     private Long invoiceNo;

     private Long bankAccountId;


}