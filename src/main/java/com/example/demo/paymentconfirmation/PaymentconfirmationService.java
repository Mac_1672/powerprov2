package com.example.demo.paymentconfirmation;

import com.example.demo.paymentconfirmation.Paymentconfirmation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentconfirmationService {
    private PaymentconfirmationRepository paymentconfirmationRepository;

    @Autowired
    public PaymentconfirmationService(PaymentconfirmationRepository repository) {
        this.paymentconfirmationRepository = repository;
    }

    public List<Paymentconfirmation> retrievePaymentconfirmation() {
        return (List<Paymentconfirmation>) paymentconfirmationRepository.findAll();
    }

    public Optional<Paymentconfirmation> retrievePaymentconfirmation(Long id) {
        return paymentconfirmationRepository.findById(id);
    }

    public List<Paymentconfirmation> retrievePaymentconfirmation(String Name) {
        return null;
    }

    public Paymentconfirmation createPaymentconfirmation(Paymentconfirmation paymentconfirmation) {

        return paymentconfirmationRepository.save(paymentconfirmation);
    }

    public Optional<Paymentconfirmation> updatePaymentconfirmation(Long id, Paymentconfirmation paymentconfirmation) {
        Optional<Paymentconfirmation> paymentconfirmationOptional = paymentconfirmationRepository.findById(id);
        if(!paymentconfirmationOptional.isPresent()) {
            return paymentconfirmationOptional;
        }

        return Optional.of(paymentconfirmationRepository.save(paymentconfirmation));
    }

    public boolean deletePaymentconfirmation(Long id) {
        try {
            paymentconfirmationRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}