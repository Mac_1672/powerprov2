package com.example.demo.paymentconfirmation;

import com.example.demo.paymentconfirmation.Paymentconfirmation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/paymentconfirmations")
public class PaymentconfirmationController {

    @Autowired
    PaymentconfirmationService paymentconfirmationService;

    @GetMapping()
    public List<Paymentconfirmation> getPaymentconfirmation() {
        return paymentconfirmationService.retrievePaymentconfirmation();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPaymentconfirmation(@PathVariable Long id) {
        Optional<Paymentconfirmation> paymentconfirmation = paymentconfirmationService.retrievePaymentconfirmation(id);
        if(!paymentconfirmation.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(paymentconfirmation);
    }

    @GetMapping("/search")
    public List<Paymentconfirmation> getPaymentconfirmation(@RequestParam(value = "Name") String Name ) {
        return paymentconfirmationService.retrievePaymentconfirmation(Name);
    }

    @PostMapping()
    public ResponseEntity<?> postPaymentconfirmation(@Valid @RequestBody Paymentconfirmation body) {
        Paymentconfirmation paymentconfirmation = paymentconfirmationService.createPaymentconfirmation(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(paymentconfirmation);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putPaymentconfirmation(@PathVariable Long id, @Valid @RequestBody Paymentconfirmation body) {
        Optional<Paymentconfirmation> paymentconfirmation = paymentconfirmationService.updatePaymentconfirmation(id, body);
        if(!paymentconfirmation.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable Long id) {
        if(!paymentconfirmationService.deletePaymentconfirmation(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}