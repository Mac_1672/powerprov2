package com.example.demo.reviewImage;

<<<<<<< HEAD
=======
import com.example.demo.category.Category;
import com.example.demo.category.CategoryDTO;
import com.example.demo.productType.ProductTypeRepository;
import com.example.demo.productType.ProductTypeService;
>>>>>>> master
import com.example.demo.review.Review;
import com.example.demo.review.ReviewRepository;
import com.example.demo.review.ReviewService;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@AllArgsConstructor
public class ReviewImageService {
    private ReviewImageRepository reviewImageRepository;
    private ReviewService reviewService;
    private ReviewRepository reviewRepository;


    public ReviewImageDTO save(ReviewImageDTO reviewImageDTO) {
        ReviewImage reviewImage = new ReviewImage();
        this.copyToEntity(reviewImage, reviewImageDTO);
        Review review = reviewRepository.findById(reviewImageDTO.getReview_id()).get();
        reviewImage.setReview(review);
        reviewImage = reviewImageRepository.save(reviewImage);
        return convertToReviewImageDTO(reviewImage);
    }

    public ReviewImageDTO convertToReviewImageDTO(ReviewImage reviewImage) {
        return ReviewImageDTO.builder()
                .id(reviewImage.getId())
                .Fullname(reviewImage.getFullname())
                .review_id(reviewImage.getReview().getId())
                .build();
    }

    public void copyToEntity(ReviewImage reviewImage, ReviewImageDTO dto) {
        reviewImage.setId(dto.getId());
        reviewImage.setFullname(dto.getFullname());
<<<<<<< HEAD
//        productTypeRepository.findById(dto.getProductTypeDTO().getId()).ifPresent(category::setProductType);
//        category.setActive(dto.getActive());
    }

    public Optional<ReviewImage>updateReviewImage(Long id, ReviewImage reviewImage) {
=======
//        productTypeRepository.findById(dto.getProductTypeDTO().getId()).ifPresent(reviewImage::setProductType);
//        reviewImage.setActive(dto.getActive());
    }

    public Optional<ReviewImage> updateReviewImage(Long id, ReviewImage reviewImage) {
>>>>>>> master
        Optional<ReviewImage> reviewImageOptional = reviewImageRepository.findById(id);
        if(!reviewImageOptional.isPresent()) {
            return reviewImageOptional;
        }

        return Optional.of(reviewImageRepository.save(reviewImage));
    }

    public boolean deleteReviewImage(Long id) {
        try {
            reviewImageRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}