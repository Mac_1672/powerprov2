package com.example.demo.category;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.*;


@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "CategoryDTO")
public class CategoryDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;

    private String Name;

    private Long product_type_id;


}