package com.example.demo.category;

import com.example.demo.productType.ProductType;
import com.example.demo.productType.ProductTypeRepository;
import com.example.demo.productType.ProductTypeService;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;


import java.util.Optional;


@Service
@AllArgsConstructor
public class CategoryService {
    private CategoryRepository categoryRepository;
    private ProductTypeService productTypeService;
    private ProductTypeRepository productTypeRepository;


    public CategoryDTO save(CategoryDTO categoryDTO) {
        Category category = new Category();
        this.copyToEntity(category, categoryDTO);
        ProductType productType = productTypeRepository.findById(categoryDTO.getProduct_type_id()).get();
        category.setProductType(productType);
        category = categoryRepository.save(category);
        return convertToCategoryDTO(category);
    }

    public CategoryDTO convertToCategoryDTO(Category category) {
        return CategoryDTO.builder()
                .id(category.getId())
                .Name(category.getName())
                .product_type_id(category.getProductType().getId())
                .build();
    }

    public void copyToEntity(Category category, CategoryDTO dto) {
        category.setId(dto.getId());
        category.setName(dto.getName());
//        productTypeRepository.findById(dto.getProductTypeDTO().getId()).ifPresent(category::setProductType);
//        category.setActive(dto.getActive());
    }

    public Optional<Category> updateCategory(Long id, Category category) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if(!categoryOptional.isPresent()) {
            return categoryOptional;
        }

        return Optional.of(categoryRepository.save(category));
    }

    public boolean deleteCategory(Long id) {
        try {
            categoryRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}