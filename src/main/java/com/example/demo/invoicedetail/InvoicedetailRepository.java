package com.example.demo.invoicedetail;

import org.springframework.data.repository.CrudRepository;


public interface InvoicedetailRepository extends CrudRepository<Invoicedetail, Long> {

}