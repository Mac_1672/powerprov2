package com.example.demo.invoicedetail;

import com.example.demo.invoicedetail.Invoicedetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvoicedetailService {
    private InvoicedetailRepository invoicedetailRepository;

    @Autowired
    public InvoicedetailService(InvoicedetailRepository repository) {
        this.invoicedetailRepository = repository;
    }

    public List<Invoicedetail> retrieveInvoicedetail() {
        return (List<Invoicedetail>) invoicedetailRepository.findAll();
    }

    public Optional<Invoicedetail> retrieveInvoicedetail(Long id) {
        return invoicedetailRepository.findById(id);
    }

    public List<Invoicedetail> retrieveInvoicedetail(String Name) {
        return null;
    }

    public Invoicedetail createInvoicedetail(Invoicedetail invoicedetail) {

        return invoicedetailRepository.save(invoicedetail);
    }

    public Optional<Invoicedetail> updateInvoicedetail(Long id, Invoicedetail invoicedetail) {
        Optional<Invoicedetail> invoicedetailOptional = invoicedetailRepository.findById(id);
        if(!invoicedetailOptional.isPresent()) {
            return invoicedetailOptional;
        }

        return Optional.of(invoicedetailRepository.save(invoicedetail));
    }

    public boolean deleteInvoicedetail(Long id) {
        try {
            invoicedetailRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}