package com.example.demo.invoicedetail;
import com.sun.istack.NotNull;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity
@Setter
@Getter
public class Invoicedetail{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 2, max = 512, message = "Please provide detail size between 2 - 512")
    private String detail;

    private Date dateTime;



}