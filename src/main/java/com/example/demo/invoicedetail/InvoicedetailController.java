package com.example.demo.invoicedetail;

import com.example.demo.invoicedetail.Invoicedetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/invoicedetails")
public class InvoicedetailController {

    @Autowired
    InvoicedetailService invoicedetailService;

    @GetMapping()
    public List<Invoicedetail> getInvoicedetail() {
        return invoicedetailService.retrieveInvoicedetail();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getInvoicedetail(@PathVariable Long id) {
        Optional<Invoicedetail> invoicedetail = invoicedetailService.retrieveInvoicedetail(id);
        if(!invoicedetail.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(invoicedetail);
    }

    @GetMapping("/search")
    public List<Invoicedetail> getInvoicedetail(@RequestParam(value = "Name") String Name ) {
        return invoicedetailService.retrieveInvoicedetail(Name);
    }

    @PostMapping()
    public ResponseEntity<?> postInvoicedetail(@Valid @RequestBody Invoicedetail body) {
        Invoicedetail invoicedetail = invoicedetailService.createInvoicedetail(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(invoicedetail);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putInvoicedetail(@PathVariable Long id, @Valid @RequestBody Invoicedetail body) {
        Optional<Invoicedetail> invoicedetail = invoicedetailService.updateInvoicedetail(id, body);
        if(!invoicedetail.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteInvoicedetail(@PathVariable Long id) {
        if(!invoicedetailService.deleteInvoicedetail(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}