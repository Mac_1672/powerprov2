package com.example.demo.billingaddress;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/billingaddress")
public class BillingaddressController {

    @Autowired
    BillingaddressService billingaddressService;

    @GetMapping()
    public List<Billingaddress> getBillingaddress() {
        return billingaddressService.retrieveBillingaddress();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getBillingaddress(@PathVariable Long id) {
        Optional<Billingaddress> billingaddress = billingaddressService.retrieveBillingaddress(id);
        if(!billingaddress.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(billingaddress);
    }

    @GetMapping("/search")
    public List<Billingaddress> getBillingaddress(@RequestParam(value = "Detail") String detail) {
        return billingaddressService.retrieveBillingaddress(detail);
    }

    @PostMapping()
    public ResponseEntity<?> postBillingaddress(@Valid @RequestBody Billingaddress body) {
        Billingaddress billingaddress = billingaddressService.createBillingaddress(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(billingaddress);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putBillingaddress(@PathVariable Long id, @Valid @RequestBody Billingaddress body) {
        Optional<Billingaddress> billingaddress = billingaddressService.updateBillingaddress(id, body);
        if(!billingaddress.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteBillingaddress(@PathVariable Long id) {
        if(!billingaddressService.deleteBillingaddress(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
