package com.example.demo.billingaddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BillingaddressService {
    private BillingaddressRepository billingaddressRepository;

    @Autowired
    public BillingaddressService(BillingaddressRepository repository) {
        this.billingaddressRepository = repository;
    }

    public List<Billingaddress> retrieveBillingaddress() {
        return (List<Billingaddress>) billingaddressRepository.findAll();
    }

    public Optional<Billingaddress> retrieveBillingaddress(Long id) {
        return billingaddressRepository.findById(id);
    }

    public List<Billingaddress> retrieveBillingaddress(String detail) {
        return null;
    }

    public Billingaddress createBillingaddress(Billingaddress billingaddress) {
        return billingaddressRepository.save(billingaddress);
    }

    public Optional<Billingaddress> updateBillingaddress(Long id, Billingaddress billingaddress) {
        Optional<Billingaddress> billingaddressOptional = billingaddressRepository.findById(id);
        if(!billingaddressOptional.isPresent()) {
            return billingaddressOptional;
        }
        return Optional.of(billingaddressRepository.save(billingaddress));
    }

    public boolean deleteBillingaddress(Long id) {
        try {
            billingaddressRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }


}