package com.example.demo.billingaddress;

import org.springframework.data.repository.CrudRepository;

public interface BillingaddressRepository extends CrudRepository<Billingaddress, Long> {

}