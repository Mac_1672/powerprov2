package com.example.demo.usertype;

import org.springframework.data.repository.CrudRepository;

public interface userTypeRepository extends CrudRepository<UserType, Long> {

}