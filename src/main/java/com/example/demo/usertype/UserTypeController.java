package com.example.demo.usertype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/usertypes")
public class UserTypeController {

    @Autowired
    UserTypeService usertypeService;

    @GetMapping()
    public List<UserType> getUserType() {
        return usertypeService.retrieveUserType();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserType(@PathVariable Long id) {
        Optional<UserType> product = usertypeService.retrieveUserType(id);
        if(!product.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(product);
    }

    @GetMapping("/search")
    public List<UserType> getUserType(@RequestParam(value = "Name") String Name ) {
        return usertypeService.retrieveProductType(Name);
    }

    @PostMapping()
    public ResponseEntity<?> postUserType(@Valid @RequestBody UserType body) {
        UserType usertype = usertypeService.createUserType(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(usertype);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putUserType(@PathVariable Long id, @Valid @RequestBody UserType body) {
        Optional<UserType> usertype = usertypeService.updateUserType(id, body);
        if(!usertype.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUserType(@PathVariable Long id) {
        if(!usertypeService.deleteUserType(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}