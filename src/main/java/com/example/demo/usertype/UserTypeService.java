package com.example.demo.usertype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserTypeService {
    private userTypeRepository usertypeRepository;

    @Autowired
    public UserTypeService(userTypeRepository repository) {
        this.usertypeRepository = repository;
    }

    public List<UserType> retrieveUserType() {
        return (List<UserType>) usertypeRepository.findAll();
    }

    public Optional<UserType> retrieveUserType(Long id) {
        return usertypeRepository.findById(id);
    }

    public List<UserType> retrieveProductType(String Name) {
        return null;
    }

    public UserType createUserType(UserType usertype) {

        return usertypeRepository.save(usertype);
    }

    public Optional<UserType> updateUserType(Long id, UserType usertype) {
        Optional<UserType> usertypeOptional = usertypeRepository.findById(id);
        if(!usertypeOptional.isPresent()) {
            return usertypeOptional;
        }

        return Optional.of(usertypeRepository.save(usertype));
    }

    public boolean deleteUserType(Long id) {
        try {
            usertypeRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}