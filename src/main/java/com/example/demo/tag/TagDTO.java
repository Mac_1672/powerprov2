package com.example.demo.tag;

import com.example.demo.productType.ProductTypeDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.*;


@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "CategoryDTO")
public class TagDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;

    private String Name;

//    private ProductTypeDTO productTypeDTO;


}