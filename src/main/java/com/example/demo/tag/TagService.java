package com.example.demo.tag;

import com.example.demo.tag.Tag;
import com.example.demo.tag.TagRepository;
import lombok.AllArgsConstructor;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;


import java.util.Optional;


@Service
@AllArgsConstructor
public class TagService {
    private TagRepository tagRepository;


    public com.example.demo.tag.TagDTO save(com.example.demo.tag.TagDTO tagDTO) {
        Tag tag = new Tag();
        this.copyToEntity(tag, tagDTO);
        tag = tagRepository.save(tag);
        return convertToTagDTO(tag);
    }

    public com.example.demo.tag.TagDTO convertToTagDTO(Tag tag) {
        return com.example.demo.tag.TagDTO.builder()
                .id(tag.getId())
                .Name(tag.getName())
                .build();
    }

    public void copyToEntity(Tag category, com.example.demo.tag.TagDTO dto) {
        category.setId(dto.getId());
        category.setName(dto.getName());
//        productTypeRepository.findById(dto.getProductTypeDTO().getId()).ifPresent(category::setProductType);
//        category.setActive(dto.getActive());
    }

    public Optional<Tag> updateTag(Long id, Tag tag) {
        Optional<Tag> tagOptional = tagRepository.findById(id);
        if(!tagOptional.isPresent()) {
            return tagOptional;
        }

        return Optional.of(tagRepository.save(tag));
    }

    public boolean deleteTag(Long id) {
        try {
            tagRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}