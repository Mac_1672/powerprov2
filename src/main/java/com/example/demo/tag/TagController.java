package com.example.demo.tag;

import com.example.demo.category.CategoryDTO;
import com.example.demo.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/tags")
public class TagController {

    @Autowired
    TagService tagService;

//    @GetMapping()
//    public List<Category> getCategory() {
//        return categoryService.retrieveCategory();
//    }
//
//    @GetMapping("/{id}")
//    public ResponseEntity<?> getCategory(@PathVariable Long id) {
//        Optional<Category> category = categoryService.retrieveCategory(id);
//        if(!category.isPresent()) {
//            return ResponseEntity.notFound().build();
//        }
//        return ResponseEntity.ok(category);
//    }
//
//    @GetMapping("/search")
//    public List<Category> getCategory(@RequestParam(value = "Name") String Name ) {
//        return categoryService.retrieveCategory(Name);
//    }

//    @PostMapping()
//    public ResponseEntity<?> postCategoryDTO(@Valid @RequestBody CategoryDTO category) {
//        Category categoryDTO = categoryService.createCategory(category);
//        return ResponseEntity.status(HttpStatus.CREATED).body(category);

//        @PostMapping()
//        public ResponseEntity<?> postCategory(@Valid @RequestBody CategoryDTO categoryDTO) {
//            CategoryDTO categoryDTO1 = categoryService.save(categoryDTO);
//            if (categoryDTO1 == null) {
//                ResponseEntity.status(HttpStatus.CREATED).body(null);
//            }else {
//                return ResponseEntity.status(HttpStatus.CREATED).body(categoryDTO1);
//            }
//            return ResponseEntity.status(HttpStatus.CREATED).body(categoryDTO1);
//        }

    @PostMapping()
        public ResponseEntity<com.example.demo.tag.TagDTO>create(@Valid @RequestBody TagDTO tagDTO) {
        tagDTO = tagService.save(tagDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(tagDTO);
    }






    @PutMapping("/{id}")
    public ResponseEntity<?> putTag(@PathVariable Long id, @Valid @RequestBody Tag body) {
        Optional<Tag> tag = tagService.updateTag(id, body);
        if(!tag.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTag(@PathVariable Long id) {
        if(!tagService.deleteTag(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}