package com.example.demo.user;
import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.net.PasswordAuthentication;
import java.util.Date;

@Data
@Entity
@Setter
@Getter

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 2, max = 512, message = "Please provide name size between 2 - 512")
    private String name;

    @NotNull
    @Email(message = "Please provide valid email address")
    private String email;

    @NotNull
    private Boolean emailVerified;

    private Integer phone;


    @NotNull
    private String password;

    private String image;

    @Column(nullable = false)
    private Boolean active;


}