package com.example.demo.shippingaddress;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/shippingaddress")
public class ShippingaddressController {

    @Autowired
    ShippingaddressService shippingaddressService;

    @GetMapping()
    public List<Shippingaddress> getShippingaddress() {
        return shippingaddressService.retrieveShippingaddress();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getShippingaddress(@PathVariable Long id) {
        Optional<Shippingaddress> shippingaddress = shippingaddressService.retrieveShippingaddress(id);
        if(!shippingaddress.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(shippingaddress);
    }

    @GetMapping("/search")
    public List<Shippingaddress> getShippingaddress(@RequestParam(value = "Name") String Name) {
        return shippingaddressService.retrieveShippingaddress(Name);
    }

    @PostMapping()
    public ResponseEntity<?> postShippingaddress(@Valid @RequestBody Shippingaddress body) {
        Shippingaddress shippingaddress = shippingaddressService.createShippingaddress(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(shippingaddress);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putShippingaddress(@PathVariable Long id, @Valid @RequestBody Shippingaddress body) {
        Optional<Shippingaddress> shippingaddress = shippingaddressService.updateShippingaddress(id, body);
        if(!shippingaddress.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteShippingaddress(@PathVariable Long id) {
        if(!shippingaddressService.deleteShippingaddress(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
