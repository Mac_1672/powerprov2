package com.example.demo.shippingaddress;

import com.example.demo.user.User;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@Entity
@Setter
@Getter

public class Shippingaddress {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 2, max = 512, message = "Please provide name size between 2 - 512")
    private String name;

    @NotNull
    @Size(min = 9, max = 10, message = "Please provide phone size between 2 - 512")
    private String phoneNumber;

    private String address;

    private Boolean isDefault;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}