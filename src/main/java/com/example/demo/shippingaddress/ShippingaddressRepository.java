package com.example.demo.shippingaddress;

import org.springframework.data.repository.CrudRepository;

public interface ShippingaddressRepository extends CrudRepository<Shippingaddress, Long> {

}