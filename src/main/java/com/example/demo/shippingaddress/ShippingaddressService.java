package com.example.demo.shippingaddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShippingaddressService {
    private ShippingaddressRepository shippingaddressRepository;

    @Autowired
    public ShippingaddressService(ShippingaddressRepository repository) {
        this.shippingaddressRepository = repository;
    }

    public List<Shippingaddress> retrieveShippingaddress() {
        return (List<Shippingaddress>) shippingaddressRepository.findAll();
    }

    public Optional<Shippingaddress> retrieveShippingaddress(Long id) {
        return shippingaddressRepository.findById(id);
    }

    public List<Shippingaddress> retrieveShippingaddress(String Name) {
        return null;
    }

    public Shippingaddress createShippingaddress(Shippingaddress shippingaddress) {
        shippingaddress.setId(null);
        return shippingaddressRepository.save(shippingaddress);
    }

    public Optional<Shippingaddress> updateShippingaddress(Long id, Shippingaddress shippingaddress) {
        Optional<Shippingaddress> shippingaddressOptional = shippingaddressRepository.findById(id);
        if(!shippingaddressOptional.isPresent()) {
            return shippingaddressOptional;
        }
        shippingaddress.setId(id);
        return Optional.of(shippingaddressRepository.save(shippingaddress));
    }

    public boolean deleteShippingaddress(Long id) {
        try {
            shippingaddressRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }


}