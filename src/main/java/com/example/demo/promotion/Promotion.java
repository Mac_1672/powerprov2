package com.example.demo.promotion;

import com.example.demo.usertype.UserType;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity
@Setter
@Getter

public class Promotion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 2, max = 512, message = "Please provide Name size between 2 - 512")
    private String Name;

    @NotNull
    private Number discountRate ;

    private Enpromotion enpromotion  ;

    @NotNull
    private Date effectiveDateFrom ;

    @NotNull
    private Date effectiveDateTo ;

    @NotNull
    private Number remains ;

    @NotNull
    private Number total;

    @NotNull
    private Number minimum ;

    //@NotNull
    //private Enum type  ;
    private enum type {PRODUCT,DELIVERY}

    @NotNull
    private Boolean  active  ;

    @ManyToOne (fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "usertype_id",nullable = false)
    private UserType usertype;

}