package com.example.demo.paymentconfirmationimage;

import org.springframework.data.repository.CrudRepository;

public interface PaymentconfirmationimageRepository extends CrudRepository<Paymentconfirmationimage, Long> {

}