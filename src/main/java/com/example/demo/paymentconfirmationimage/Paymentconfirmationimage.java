package com.example.demo.paymentconfirmationimage;

import com.example.demo.paymentconfirmation.Paymentconfirmation;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Setter
@Getter

public class Paymentconfirmationimage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String fileName;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "paymentconfirmation_id", nullable = false)
    private Paymentconfirmation paymentconfirmation;
}