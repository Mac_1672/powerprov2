package com.example.demo.paymentconfirmationimage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Paymentconfirmationimages")
public class PaymentconfirmationimageController {

    @Autowired
    PaymentconfirmationimageService PaymentconfirmationimageService;

    @GetMapping()
    public List<Paymentconfirmationimage> getPaymentconfirmationimage() {
        return PaymentconfirmationimageService.retrievePaymentconfirmationimage();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPaymentconfirmationimage(@PathVariable Long id) {
        Optional<Paymentconfirmationimage> Paymentconfirmationimage = PaymentconfirmationimageService.retrievePaymentconfirmationimage(id);
        if(!Paymentconfirmationimage.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(Paymentconfirmationimage);
    }

    @GetMapping("/search")
    public List<Paymentconfirmationimage> getPaymentconfirmationimage(@RequestParam(value = "Name") String Name ) {
        return PaymentconfirmationimageService.retrievePaymentconfirmationimage(Name);
    }

    @PostMapping()
    public ResponseEntity<?> postPaymentconfirmationimage(@Valid @RequestBody Paymentconfirmationimage body) {
        Paymentconfirmationimage Paymentconfirmationimage = PaymentconfirmationimageService.createPaymentconfirmationimage(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(Paymentconfirmationimage);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putPaymentconfirmationimage(@PathVariable Long id, @Valid @RequestBody Paymentconfirmationimage body) {
        Optional<Paymentconfirmationimage> Paymentconfirmationimage = PaymentconfirmationimageService.updatePaymentconfirmationimage(id, body);
        if(!Paymentconfirmationimage.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable Long id) {
        if(!PaymentconfirmationimageService.deletePaymentconfirmationimage(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}