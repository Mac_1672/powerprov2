package com.example.demo.paymentconfirmationimage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentconfirmationimageService {
    private PaymentconfirmationimageRepository paymentconfirmationimageRepository;

    @Autowired
    public PaymentconfirmationimageService(PaymentconfirmationimageRepository repository) {
        this.paymentconfirmationimageRepository = repository;
    }

    public List<Paymentconfirmationimage> retrievePaymentconfirmationimage() {
        return (List<Paymentconfirmationimage>) paymentconfirmationimageRepository.findAll();
    }

    public Optional<Paymentconfirmationimage> retrievePaymentconfirmationimage(Long id) {
        return paymentconfirmationimageRepository.findById(id);
    }

    public List<Paymentconfirmationimage> retrievePaymentconfirmationimage(String Name) {
        return null;
    }

    public Paymentconfirmationimage createPaymentconfirmationimage(Paymentconfirmationimage paymentconfirmationimage) {

        return paymentconfirmationimageRepository.save(paymentconfirmationimage);
    }

    public Optional<Paymentconfirmationimage> updatePaymentconfirmationimage(Long id, Paymentconfirmationimage paymentconfirmationimage) {
        Optional<Paymentconfirmationimage> paymentconfirmationimageOptional = paymentconfirmationimageRepository.findById(id);
        if(!paymentconfirmationimageOptional.isPresent()) {
            return paymentconfirmationimageOptional;
        }

        return Optional.of(paymentconfirmationimageRepository.save(paymentconfirmationimage));
    }

    public boolean deletePaymentconfirmationimage(Long id) {
        try {
            paymentconfirmationimageRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}