package com.example.demo.appointment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AppointmentService {
    private AppointmentRepository appointmentRepository;

    @Autowired
    public AppointmentService(AppointmentRepository repository) {
        this.appointmentRepository = repository;
    }

    public List<Appointment> retrieveAppointment() {
        return (List<Appointment>) appointmentRepository.findAll();
    }

    public Optional<Appointment> retrieveAppointment(Long id) {
        return appointmentRepository.findById(id);
    }

    public List<Appointment> retrieveAppointment(String detail) {
        return null;
    }

    public Appointment createAppointment(Appointment appointment) {
        return appointmentRepository.save(appointment);
    }

    public Optional<Appointment> updateAppointment(Long id, Appointment appointment) {
        Optional<Appointment> appointmentOptional = appointmentRepository.findById(id);
        if(!appointmentOptional.isPresent()) {
            return appointmentOptional;
        }
        return Optional.of(appointmentRepository.save(appointment));
    }

    public boolean deleteAppointment(Long id) {
        try {
            appointmentRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }


}