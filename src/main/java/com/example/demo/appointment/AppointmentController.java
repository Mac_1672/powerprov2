package com.example.demo.appointment;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/appointments")
public class AppointmentController {

    @Autowired
    AppointmentService appointmentService;

    @GetMapping()
    public List<Appointment> getAppointment() {
        return appointmentService.retrieveAppointment();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAppointment(@PathVariable Long id) {
        Optional<Appointment> appointment = appointmentService.retrieveAppointment(id);
        if(!appointment.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(appointment);
    }

    @GetMapping("/search")
    public List<Appointment> getAppointment(@RequestParam(value = "Detail") String detail) {
        return appointmentService.retrieveAppointment(detail);
    }

    @PostMapping()
    public ResponseEntity<?> postAppointment(@Valid @RequestBody Appointment body) {
        Appointment appointment = appointmentService.createAppointment(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(appointment);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putAppointment(@PathVariable Long id, @Valid @RequestBody Appointment body) {
        Optional<Appointment> appointment = appointmentService.updateAppointment(id, body);
        if(!appointment.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAppointment(@PathVariable Long id) {
        if(!appointmentService.deleteAppointment(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
