package com.example.demo.bankaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BankaccountService {
    private BankaccountRepository bankaccountRepository;

    @Autowired
    public BankaccountService(BankaccountRepository repository) {
        this.bankaccountRepository = repository;
    }

    public List<Bankaccount> retrieveBankaccount() {
        return (List<Bankaccount>) bankaccountRepository.findAll();
    }

    public Optional<Bankaccount> retrieveBankaccount(Long id) {
        return bankaccountRepository.findById(id);
    }

    public List<Bankaccount> retrieveBankaccount(String Name) {
        return null;
    }

    public Bankaccount createBankaccount(Bankaccount bankaccount) {

        return bankaccountRepository.save(bankaccount);
    }

    public Optional<Bankaccount> updateBankaccount(Long id, Bankaccount bankaccount) {
        Optional<Bankaccount> bankaccountOptional = bankaccountRepository.findById(id);
        if(!bankaccountOptional.isPresent()) {
            return bankaccountOptional;
        }

        return Optional.of(bankaccountRepository.save(bankaccount));
    }

    public boolean deleteBankaccount(Long id) {
        try {
            bankaccountRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}