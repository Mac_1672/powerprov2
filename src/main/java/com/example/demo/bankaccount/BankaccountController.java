package com.example.demo.bankaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/bankaccounts")
public class BankaccountController {

    @Autowired
    BankaccountService bankaccountService;

    @GetMapping()
    public List< Bankaccount> getBankaccount() {
        return bankaccountService.retrieveBankaccount();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getBankaccount(@PathVariable Long id) {
        Optional< Bankaccount> bankaccount = bankaccountService.retrieveBankaccount(id);
        if(!bankaccount.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(bankaccount);
    }

    @GetMapping("/search")
    public List< Bankaccount> getBankaccount(@RequestParam(value = "Name") String Name ) {
        return bankaccountService.retrieveBankaccount(Name);
    }

    @PostMapping()
    public ResponseEntity<?> postBankaccount(@Valid @RequestBody  Bankaccount body) {
        Bankaccount bankaccount = bankaccountService.createBankaccount(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(bankaccount);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putBankaccount(@PathVariable Long id, @Valid @RequestBody  Bankaccount body) {
        Optional< Bankaccount> bankaccount = bankaccountService.updateBankaccount(id, body);
        if(!bankaccount.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteBankaccount(@PathVariable Long id) {
        if(!bankaccountService.deleteBankaccount(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}