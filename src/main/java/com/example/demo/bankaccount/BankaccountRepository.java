package com.example.demo.bankaccount;

import org.springframework.data.repository.CrudRepository;

public interface BankaccountRepository extends CrudRepository<Bankaccount, Long> {

}